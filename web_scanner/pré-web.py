#!/usr/bun/env python3
# coding:utf8
import sys
import urllib.parse

import mechanize
from bs4 import BeautifulSoup
from urllib.parse import urlparse

class WebCrawler:


    def __init__(self, url, proxy=None, user_agent="Mozilla/5.0 (Windows NT 10.0; rv:91.0) Gecko/20100101 Firefox/91.0"):
        """

        :param url:
        :param proxy:
        :param user_agent:
        """
        if url.endswith("/"):
            self.url = url.rstrip("/")
        else:
            self.url = url
        self.proxy = proxy
        self.user_agent = user_agent
        self.browser = mechanize.Browser()
        self.link_list = []
        self.stopped = False


    def print_link_list(self):
        """

        :return:
        """
        for link in self.link_list:
            print(link)


    def get_page_source(self, page=None):
        """

        :param page:
        :return:
        """
        if page is None:
            page = self.url
        self.browser.set_handle_robots(False)
        user_agent = [("User-agent", self.user_agent)]
        self.browser.addheaders = user_agent
        if self.proxy:
            self.browser.set_proxies(self.proxy)
        page = page.strip()
        try:
            res = self.browser.open(page)
        except Exception as e:
            print("Erreur pour la page : " + page + " " + str(e))
            return None
        return res


    def get_page_links(self, page=None):
        """

        :param page:
        :return:
        """
        link_list = []
        if page is None:
            page = self.url
        source = self.get_page_source(page)
        if source is not None:
            soup = BeautifulSoup(source, "html.parser")
            uparse = urlparse(page)
            for link in soup.find_all("a"):
                if not link.get("href") is None:
                    href = link.get("href")
                    if "#" in href:
                        href = href.split("#")[0]
                    new_link = urllib.parse.urljoin(page, href)
                    if uparse.hostname in new_link and new_link not in link_list:
                        link_list.append(new_link)

                    # print("link added to the list : " + href)
            return link_list
        else:
            return []


    def print_cookies(self):
        """

        :return:
        """
        for cookie in self.browser.cookiejar:
            print(cookie)


    def get_cookies(self):
        """

        :return:
        """
        cookies_list = []
        for cookie in self.browser.cookiejar:
            cookies_list.append(cookie)
        return cookies_list


    def crawl(self, page=None):
        """

        :param page:
        :return:
        """
        try:
            page_links = self.get_page_links(page)
            for link in page_links:
                if self.stopped:
                    break
                if link not in self.link_list:
                    self.link_list.append(link)
                    print("Link added to the list : " + link)
                    self.crawl(link)

        except KeyboardInterrupt:
            print("\nProgramm interrupted by user.")
            sys.exit(1)
        except Exception as e:
            print("Error : " + str(e))
            sys.exit(1)