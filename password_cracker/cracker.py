import urllib.request
import urllib.response
import urllib.error
import string
import sys
import hashlib
from utils import *


class Cracker:
    @staticmethod
    def crack_dict(md5, file, order, done_queue):
        """
        Casse un HASH MD5 (md5) via une liste de mot clé (file)
        :param md5: hash MD5 à casser
        :param file: Fichier de mots-clés à utiliser
        :return:
        """


        try:
            trouve = False
            ofile = open(file, "r")
            if Order.ASCEND:
                contenu = reversed(list(ofile.readlines()))
            else:
                contenu = ofile.readlines()

            for mot in contenu:
                mot = mot.strip("\n").encode("utf8")
                hashmd5 = hashlib.md5(mot).hexdigest()
                if hashmd5 == md5:
                    print(Couleur.VERT + "[+] PASSWORD FOUND : " + str(mot) + " (" + hashmd5 + ")" + Couleur.FIN)
                    trouve = True
                    done_queue.put("TROUVE")
                    break
            if not trouve:
                print(Couleur.ROUGE + "[-] PASSWORD NOT FOUND" + Couleur.FIN)
                done_queue.put("NON TROUVE")
            ofile.close()
        except FileNotFoundError:
            print(Couleur.ROUGE + "[-] Error : FILE NOT FOUND !" + Couleur.FIN)
            sys.exit(1)
        except Exception as err:
            print(Couleur.ROUGE + "[-] Error : " + str(err) + Couleur.FIN)
            sys.exit(2)

    @staticmethod
    def crack_incr(md5, length, _currpass=[]):
        """
        Casse un HASH MD5 via une méthode incrémentale pour un mdp de longeur length
        :param md5: hash MD5 à casser
        :param length: longueur présumer du mot de passe
        :param currpass: liste temporaire automatiquement utilisée via récursion contenant l'essai de mdp actuel
        :return:
        """
        # lettres = string.ascii_letters
        lettres = string.printable
        if length >=1:
            if len(_currpass) == 0:
                currpass = ['a' for _ in range(length)]
                Cracker.crack_incr(md5, length, currpass)
            else:
                for c in lettres:
                    _currpass[length - 1] = c
                    currhash = hashlib.md5("".join(_currpass).encode("utf8")).hexdigest()
                    print("Trying : " + "".join(_currpass) + " (" + currhash + ")")
                    if currhash == md5:
                        print(Couleur.VERT + "PASSWORD FOUND ! " + "".join(_currpass) + Couleur.FIN)
                        sys.exit(0)
                    else:
                        Cracker.crack_incr(md5, length -1, _currpass)

    @staticmethod
    def crack_online(md5):
        """
        search online
        :param md5: hash MD5 à casser
        :return:
        """
        try:
            user_agent = "Mozilla/5.0 (Windows; U; Windows NT5.1; fr-FR; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7"
            headers = {'User-Agent': user_agent}
            #url = "https://www.google.fr/search?hl=fr&q=" + md5
            url = "https://www.qwant.com/?q=" + md5
            requete = urllib.request.Request(url, None, headers)
            reponse = urllib.request.urlopen(requete)
        except urllib.error.HTTPError as e:
            print("Erreur HTTP : " + e.code)
        except urllib.error.URLError as e:
            print("Erreur d'URL : " + e.reason)
        print(reponse.read())
        if "Aucun document" in str(reponse.read()):
            print(Couleur.ROUGE + "[-] HASH NOT FOUND" + Couleur.FIN)
        else:
            print(Couleur.VERT + "[+] PASSWORD FOUND : " + url + Couleur.FIN)

    @staticmethod
    def crack_smart(md5, pattern, _index=0):
        """

        :param md5:
        :param pattern:
        :param _index:
        :return:
        """
        MAJ = string.ascii_uppercase
        MIN = string.ascii_lowercase
        CHIFFRE = string.digits

        if _index < len(pattern):
            if pattern[_index] == MAJ or CHIFFRE or MIN:
                Cracker.crack_smart(md5, pattern, _index + 1)
            if "^" == pattern[_index]:
                for c in MAJ:
                    p = pattern.replace("^", c, 1)
                    currhash = hashlib.md5(p.encode("utf8")).hexdigest()
                    if currhash == md5:
                        print(Couleur.VERT + "[+] PASSWORD FOUND : " + p + Couleur.FIN)
                        sys.exit(0)
                    print("MAJ : " + p + " (" + currhash + ")")
                    Cracker.crack_smart(md5, p, _index + 1)
            if "*" == pattern[_index]:
                for c in MIN:
                    p = pattern.replace("*", c, 1)
                    currhash = hashlib.md5(p.encode("utf8")).hexdigest()
                    if currhash == md5:
                        print(Couleur.VERT + "[+] PASSWORD FOUND : " + p + Couleur.FIN)
                        sys.exit(0)
                    print("MIN : " + p + " (" + currhash + ")")
                    Cracker.crack_smart(md5, p, _index + 1)
            if "°" == pattern[_index]:
                for c in CHIFFRE:
                    p = pattern.replace("°", c, 1)
                    currhash = hashlib.md5(p.encode("utf8")).hexdigest()
                    if currhash == md5:
                        print(Couleur.VERT + "[+] PASSWORD FOUND : " + p + Couleur.FIN)
                        sys.exit(0)
                    print("CHIFFRE : " + p + " (" + currhash + ")")
                    Cracker.crack_smart(md5, p, _index + 1)
        else:
            return


    @staticmethod
    def work(work_queue, done_queue, md5, file, order):
        """

        :param work_queue:
        :param done_queue:
        :param md5:
        :param file:
        :param order:
        :return:
        """
        o = work_queue.get()
        o.crack_dict(md5, file, order, done_queue)