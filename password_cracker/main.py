#! /usr/bin/env python3
# coding:utf-8

import time
import argparse
import atexit
import multiprocessing
from cracker import *




def display_time():
    print("Time : " + str(time.time() - debut) + " seconds")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Password Cracker")
    parser.add_argument("-f", "--file", dest="file", help="Path of the dictionnary file", required=False)
    parser.add_argument("-g", "--gen", dest="gen", help="Generate MD5 hash of password", required=False)
    parser.add_argument("-md5", dest="md5", help="Hashed password (MD5)", required=False)
    parser.add_argument("-l", dest="plength", help="Password length", required=False, type=int)
    parser.add_argument("-o", dest="online", help="search hash online (google)", required=False, action="store_true")
    parser.add_argument("-p", dest="pattern", help="Use a pattern")

    args = parser.parse_args()

    processes = []
    work_queue = multiprocessing.Queue()
    done_queue = multiprocessing.Queue()
    cracker = Cracker()

    debut = time.time()
    atexit.register(display_time)
    if args.md5:
        print("[*] CRACKING HASH " + args.md5)
        if args.file:
            print("[*] USING DICTIONNARY FILE" + args.md5)
            p1 = multiprocessing.Process(target=Cracker.work, args=(work_queue, done_queue, args.md5, args.file, False))
            processes.append(p1)
            work_queue.put(cracker)
            p1.start()
            p2 = multiprocessing.Process(target=Cracker.work, args=(work_queue, done_queue, args.md5, args.file, True))
            processes.append(p2)
            work_queue.put(cracker)
            p2.start()
            nontrouve = 0
            while True:
                data = done_queue.get()

                if data == "TROUVE" or data == "NON TROUVE":
                    p1.kill()
                    p2.kill()
                    break
                elif data == "NON TROUVE":
                    nontrouve += 1
                if nontrouve == len(processes):
                    print("Aucun processus n'a trouvé le mot de passe")
                    break

            #Cracker.crack_dict(args.md5, args.file)
        elif args.plength:
            print("[*] USING INCREMENTAL MODE FOR " + str(args.plength) + " letter(s)")
            Cracker.crack_incr(args.md5, args.plength)
        elif args.online:
            print("[*] USING ONLINE SEARCH" )
            Cracker.crack_online(args.md5)
        elif args.pattern:
            print("[*] USING A PATTERN")
            Cracker.crack_smart(args.md5, args.pattern)
        else:
            print("Please choose -f or -l argument")
    else:
        print("MD5 hash not provided")
    if args.gen:
        print("[*] MD5 HASH OF " + args.gen + " : " + hashlib.md5(args.gen.encode("utf8")).hexdigest())

