#! /usr/bin/env python3

import random
import sys
import time
import string
import hashlib

mot_de_passe = input("Quel est votre mot de passe : ")  # le mot de passe à trouver
mot_de_passe_md5 = hashlib.md5(mot_de_passe.encode("utf8")).hexdigest()
print(mot_de_passe_md5)


def hash_crack():
    try:
        mots_fr = open("/home/sargosse/liste_francais.txt", "r")
        trouve = False
        for mot in mots_fr.readlines():
            mot = mot.strip("\n").encode("utf8")
            hashmd5 = hashlib.md5(mot).hexdigest()
            if hashmd5 == mot_de_passe_md5:
                print("Mot de passe trouvé : " + str(mot) + " (" + hashmd5 + ")")
                trouve = True
        if not trouve:
            print("Mot de passe non trouvé")
        mots_fr.close()
    except FileNotFoundError:
        print("Erreur : nom de dossier ou fichier introuvable !")
        sys.exit(1)
    except Exception as err:
        print("Erreur : " + str(err))




def mot_aleatoire():
    lettres = string.ascii_letters
    suiv = ""
    resultat = ""
    for i in range(len(mot_de_passe)):
        while mot_de_passe[i] != suiv:
            print(resultat + suiv)
            suiv = random.choice(lettres)
        resultat += suiv
    return resultat


debut = time.time()
hash_crack()
print("Durée : " + str(time.time() - debut) + " secondes")